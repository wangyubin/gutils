package logger

import (
	"os"
	"testing"
)

func TestConsoleLogger(t *testing.T) {
	myLogger := NewLogger(LogConfig{
		ConsoleEnabled: true,
		FileEnabled:    false,
		Level:          Debug,
	})

	myLogger.Debug().Str("key", "val").Msg("This is debug log")
	myLogger.Info().Msg("This is info log")
	myLogger.Warn().Msg("This is warn log")
	myLogger.Error().Msg("This is error log")

}

func TestFileLogger(t *testing.T) {
	myLogger := NewLogger(LogConfig{
		ConsoleEnabled: false,
		FileEnabled:    true,
		Level:          Debug,
		LogDir:         "/tmp",
		FileName:       "test.log",
	})

	myLogger.Debug().Str("key", "val").Msg("This is debug log")
	myLogger.Info().Msg("This is info log")
	myLogger.Warn().Msg("This is warn log")
	myLogger.Error().Msg("This is error log")

	os.Remove("/tmp/test.log")
}

func TestAllLogger(t *testing.T) {
	myLogger := NewLogger(LogConfig{
		ConsoleEnabled: true,
		FileEnabled:    true,
		Level:          Debug,
		LogDir:         "/tmp",
		FileName:       "test.log",
	})

	myLogger.Debug().Str("key", "val").Msg("This is debug log")
	myLogger.Info().Msg("This is info log")
	myLogger.Warn().Msg("This is warn log")
	myLogger.Error().Msg("This is error log")

	os.Remove("/tmp/test.log")
}
