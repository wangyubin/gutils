package parsers

import (
	"fmt"
	"testing"
)

func TestParseFloat64WithDefault(t *testing.T) {
	var tests = []struct {
		param1 string
		param2 float64
		expect float64
	}{
		{"1.1", 0, 1.1},
		{"-1.1", 0, -1.1},
		{"1.1.1", 0, 0},
	}

	for _, test := range tests {
		testname := fmt.Sprintf("ParseFloat64WithDefault/(%s, %f)", test.param1, test.param2)
		t.Run(testname, func(t *testing.T) {
			ret := ParseFloat64WithDefault(test.param1, test.param2)
			if test.expect != ret {
				t.Errorf("result is: %f, expect is: %f\n", ret, test.expect)
			}
		})
	}
}

func TestParseIntWithDefault(t *testing.T) {
	var tests = []struct {
		param1 string
		param2 int
		expect int
	}{
		{"1", 0, 1},
		{"-1", 0, -1},
		{"1.1.1", 0, 0},
	}

	for _, test := range tests {
		testname := fmt.Sprintf("ParseFloat64WithDefault/(%s, %d)", test.param1, test.param2)
		t.Run(testname, func(t *testing.T) {
			ret := ParseIntWithDefault(test.param1, test.param2)
			if test.expect != ret {
				t.Errorf("result is: %d, expect is: %d\n", ret, test.expect)
			}
		})
	}
}

func TestFloat64String(t *testing.T) {
	var tests = []struct {
		param1 float64
		expect string
	}{
		{0.1, "0.1"},
		{1.0100, "1.01"},
		{2.00, "2"},
	}

	for _, test := range tests {
		testname := fmt.Sprintf("Float64String/(%f)", test.param1)
		t.Run(testname, func(t *testing.T) {
			ret := Float64String(test.param1)
			if test.expect != ret {
				t.Errorf("result is: %s, expect is: %s\n", ret, test.expect)
			}
		})
	}
}
