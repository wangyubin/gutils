package des

import (
	"crypto/cipher"
	"crypto/des"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"log"
	"runtime"

	"gitee.com/wangyubin/gutils/cryptos"
)

func TripleDesEncrypt(plainText, secretKey, ivDes []byte) ([]byte, error) {
	if len(secretKey) != 24 {
		return nil, fmt.Errorf("secretKey length must be 24")
	}
	block, err := des.NewTripleDESCipher(secretKey)
	if err != nil {
		return nil, err
	}
	paddingText := cryptos.PKCS5Padding(plainText, block.BlockSize())

	var iv []byte
	if len(ivDes) != 0 {
		if len(ivDes) != block.BlockSize() {
			return nil, fmt.Errorf("ivDes length invalid")
		} else {
			iv = ivDes
		}
	} else {
		iv = []byte(cryptos.Ivdes)
	}
	blockMode := cipher.NewCBCEncrypter(block, iv)

	cipherText := make([]byte, len(paddingText))
	blockMode.CryptBlocks(cipherText, paddingText)
	return cipherText, nil
}

func TripleDesDecrypt(cipherText, secretKey, ivDes []byte) (plainText []byte, err error) {
	if len(secretKey) != 24 {
		return nil, fmt.Errorf("secretKey length must be 24")
	}
	block, err := des.NewTripleDESCipher(secretKey)
	if err != nil {
		return nil, err
	}

	defer func() {
		if err := recover(); err != nil {
			switch err.(type) {
			case runtime.Error:
				log.Println("runtime err:", err, "Check that the key is correct")
			default:
				log.Println("error:", err)
			}
		}
	}()

	var iv []byte
	if len(ivDes) != 0 {
		if len(ivDes) != block.BlockSize() {
			return nil, fmt.Errorf("ivDes length invalid")
		} else {
			iv = ivDes
		}
	} else {
		iv = []byte(cryptos.Ivdes)
	}
	blockMode := cipher.NewCBCDecrypter(block, iv)

	paddingText := make([]byte, len(cipherText)) //
	blockMode.CryptBlocks(paddingText, cipherText)

	plainText, err = cryptos.PKCS5UnPadding(paddingText, block.BlockSize())
	if err != nil {
		return nil, err
	}
	return plainText, nil
}

func TripleDesEncryptBase64(plainText, secretKey, ivAes []byte) (cipherTextBase64 string, err error) {
	encryBytes, err := TripleDesEncrypt(plainText, secretKey, ivAes)
	return base64.StdEncoding.EncodeToString(encryBytes), err
}

func TripleDesEncryptHex(plainText, secretKey, ivAes []byte) (cipherTextHex string, err error) {
	encryBytes, err := TripleDesEncrypt(plainText, secretKey, ivAes)
	return hex.EncodeToString(encryBytes), err
}

func TripleDesDecryptByBase64(cipherTextBase64 string, secretKey, ivAes []byte) (plainText []byte, err error) {
	plainTextBytes, err := base64.StdEncoding.DecodeString(cipherTextBase64)
	if err != nil {
		return []byte{}, err
	}
	return TripleDesDecrypt(plainTextBytes, secretKey, ivAes)
}

func TripleDesDecryptByHex(cipherTextHex string, secretKey, ivAes []byte) (plainText []byte, err error) {
	plainTextBytes, err := hex.DecodeString(cipherTextHex)
	if err != nil {
		return []byte{}, err
	}
	return TripleDesDecrypt(plainTextBytes, secretKey, ivAes)
}
