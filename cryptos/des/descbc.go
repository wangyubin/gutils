package des

import (
	"crypto/cipher"
	"crypto/des"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"log"
	"runtime"

	"gitee.com/wangyubin/gutils/cryptos"
)

func DesCbcEncrypt(plainText, secretKey, ivDes []byte) ([]byte, error) {
	if len(secretKey) != 8 {
		return nil, fmt.Errorf("secretKey length must be 8")
	}
	block, err := des.NewCipher(secretKey)
	if err != nil {
		return nil, err
	}
	paddingText := cryptos.PKCS5Padding(plainText, block.BlockSize())

	var iv []byte
	if len(ivDes) != 0 {
		if len(ivDes) != block.BlockSize() {
			return nil, fmt.Errorf("ivDes length invalid")
		} else {
			iv = ivDes
		}
	} else {
		iv = []byte(cryptos.Ivdes)
	} // Initialization vector
	blockMode := cipher.NewCBCEncrypter(block, iv)

	cipherText := make([]byte, len(paddingText))
	blockMode.CryptBlocks(cipherText, paddingText)
	return cipherText, nil
}

func DesCbcDecrypt(cipherText, secretKey, ivDes []byte) ([]byte, error) {
	if len(secretKey) != 8 {
		return nil, fmt.Errorf("secretKey length must be 8")
	}
	block, err := des.NewCipher(secretKey)
	if err != nil {
		return nil, err
	}

	defer func() {
		if err := recover(); err != nil {
			switch err.(type) {
			case runtime.Error:
				log.Println("runtime err:", err, "Check that the key is correct")
			default:
				log.Println("error:", err)
			}
		}
	}()

	var iv []byte
	if len(ivDes) != 0 {
		if len(ivDes) != block.BlockSize() {
			return nil, fmt.Errorf("ivDes length invalid")
		} else {
			iv = ivDes
		}
	} else {
		iv = []byte(cryptos.Ivdes)
	} // Initialization vector
	blockMode := cipher.NewCBCDecrypter(block, iv)

	plainText := make([]byte, len(cipherText))
	blockMode.CryptBlocks(plainText, cipherText)

	unPaddingText, err := cryptos.PKCS5UnPadding(plainText, block.BlockSize())
	if err != nil {
		return nil, err
	}
	return unPaddingText, nil
}

func DesCbcEncryptBase64(plainText, secretKey, ivAes []byte) (cipherTextBase64 string, err error) {
	encryBytes, err := DesCbcEncrypt(plainText, secretKey, ivAes)
	return base64.StdEncoding.EncodeToString(encryBytes), err
}

func DesCbcEncryptHex(plainText, secretKey, ivAes []byte) (cipherTextHex string, err error) {
	encryBytes, err := DesCbcEncrypt(plainText, secretKey, ivAes)
	return hex.EncodeToString(encryBytes), err
}

func DesCbcDecryptByBase64(cipherTextBase64 string, secretKey, ivAes []byte) (plainText []byte, err error) {
	plainTextBytes, err := base64.StdEncoding.DecodeString(cipherTextBase64)
	if err != nil {
		return []byte{}, err
	}
	return DesCbcDecrypt(plainTextBytes, secretKey, ivAes)
}

func DesCbcDecryptByHex(cipherTextHex string, secretKey, ivAes []byte) (plainText []byte, err error) {
	plainTextBytes, err := hex.DecodeString(cipherTextHex)
	if err != nil {
		return []byte{}, err
	}
	return DesCbcDecrypt(plainTextBytes, secretKey, ivAes)
}
