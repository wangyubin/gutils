package gutils

import (
	"fmt"
	"io/ioutil"
	"os/exec"
)

func createScript(script string) (string, error) {
	// 生成临时文件
	file, err := ioutil.TempFile("", "tmp-script")
	if err != nil {
		return "", err
	}
	defer file.Close()

	if _, err = file.WriteString(script); err != nil {
		return "", err
	}

	return file.Name(), nil
}

// ExecScript 执行脚本
func ExecScript(script string) ([]byte, error) {
	// 生成临时文件
	fp, err := createScript(script)
	if err != nil {
		return nil, err
	}

	var cmd = exec.Command("/bin/bash", fp)
	return cmd.Output()
}

// 后台执行命令
func ExecScriptBG(script string) error {
	// 生成临时文件
	fp, err := createScript(script)
	if err != nil {
		return err
	}

	var cmd = exec.Command("/bin/bash", fp)
	return cmd.Run()
}

// 关闭某个进程
func KillProcess(name string, force bool) error {
	times := 3 // 默认尝试3次关闭进程
	var err error

	// 查询进程是否存在
	if !IsProcessExisted(name) {
		return nil
	}
	for i := 0; i < times; i++ {
		// 关闭进程
		cmd := fmt.Sprintf("pgrep %s | xargs kill", name)
		_, err = ExecScript(cmd)

		// 查询进程是否存在
		if !IsProcessExisted(name) {
			return nil
		}
	}

	return err
}

// 查询进程是否正常运行
func IsProcessExisted(name string) bool {
	// 查询进程是否存在
	cmd := fmt.Sprintf(`pgrep %s`, name)
	_, err := ExecScript(cmd)
	return err == nil
}
