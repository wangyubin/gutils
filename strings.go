package gutils

import (
	"math/rand"
	"time"
)

func RandomPasswd(length int) string {
	rand.Seed(time.Now().UnixNano())
	numStr := "0123456789"
	charStr := "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

	sourceStr := numStr + charStr
	var passwd []byte = make([]byte, length, length)
	for i := 0; i < length; i++ {
		index := rand.Intn(len(sourceStr))
		passwd[i] = sourceStr[index]
	}

	return string(passwd)
}

func RemoveDuplicatedArrs(arr1, arr2 []string) []string {
	m := make(map[string]struct{})

	for _, arr := range arr1 {
		m[arr] = struct{}{}
	}
	for _, arr := range arr2 {
		m[arr] = struct{}{}
	}

	arr := make([]string, 0)
	for k := range m {
		arr = append(arr, k)
	}

	return arr
}

// IsStrInArr 判断数组 arr 中是否包含 s 元素
func IsStrInArr(s string, arr []string) bool {
	for _, a := range arr {
		if s == a {
			return true
		}
	}

	return false
}
