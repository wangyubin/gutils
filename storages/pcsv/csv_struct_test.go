package pcsv

import (
	"fmt"
	"testing"
)

type csvRow struct {
	Id    int     `pcsv:"0,2"`
	Score float64 `pcsv:"2,0"`
	Name  string  `pcsv:"1"`
}

func TestCsv2Struct(t *testing.T) {
	filePaths := prepareCsv()
	pr := NewPCsvReader(filePaths, false)
	pr.FieldsPerRecord = -1
	defer cleanCsv(filePaths)

	dataCh := make(chan []string, 1)
	go pr.Read(dataCh)

	var r csvRow
	for data := range dataCh {
		err := Unmarshal(data, &r)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Printf("record: %#v\n", r)
	}
}

func TestStruct2Csv(t *testing.T) {
	rows := []csvRow{
		{Id: 1, Name: "test01", Score: 10.01},
		{Id: 2, Name: "test02", Score: 20.02},
		{Id: 3, Name: "test03", Score: 30.03},
		{Id: 4, Name: "test04", Score: 40.04},
	}

	for _, row := range rows {
		data, _ := Marshal(&row)
		fmt.Println(data)
	}
}
