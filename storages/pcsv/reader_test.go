package pcsv

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strconv"
	"testing"
)

func TestRead(t *testing.T) {
	filePaths := prepareCsv()
	pr := NewPCsvReader(filePaths, false)
	pr.FieldsPerRecord = -1
	defer cleanCsv(filePaths)

	dataCh := make(chan []string, 1)
	go pr.Read(dataCh)

	for data := range dataCh {
		fmt.Printf("record: %v\n", data)
		for k, v := range pr.ReadingState {
			fmt.Printf("pr state: [%s] LINE: %d\n", k, v)
		}
	}
}

func prepareCsv() []string {
	fps := make([]string, 0)
	fps = append(fps, "./test1.csv")
	fps = append(fps, "./test2.csv")

	for i, fp := range fps {
		fo, _ := os.Create(fp)
		csvWriter := csv.NewWriter(fo)
		err := csvWriter.WriteAll([][]string{
			{strconv.Itoa(i), "test01", "30.01"},
			{strconv.Itoa(i), "test02", "20.2"},
			{strconv.Itoa(i), "test02", "20", "33"},
			{strconv.Itoa(i), "test02", "20.33"},
			{strconv.Itoa(i), "test02", "20.12"},
			{strconv.Itoa(i), "test02", "20.3"},
			{strconv.Itoa(i), "test02", "20.2"},
			{strconv.Itoa(i), "test02", "20.67"},
		})
		if err != nil {
			log.Fatal(err)
		}

		fo.Close()
	}

	return fps
}

func cleanCsv(filePaths []string) {
	for _, fp := range filePaths {
		os.Remove(fp)
	}
}
