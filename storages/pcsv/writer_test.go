package pcsv

import (
	"io/ioutil"
	"os"
	"path"
	"strconv"
	"strings"
	"testing"
)

func TestWrite(t *testing.T) {
	filePrefix := "test"
	outputDir := "/home/wangyubin/tmp/pcsvWrite"
	err := os.Mkdir(outputDir, 0755)
	if err != nil {
		t.Fatal(err)
	}
	pw := NewPCsvWriter(filePrefix, outputDir)
	pw.MaxLines = 2
	pw.Title = []string{"no", "content"}
	defer cleanWriteCsv(outputDir)

	dataCh := make(chan []string, 10)
	go func() {
		defer close(dataCh)
		for i := 0; i < 91; i++ {
			dataCh <- []string{strconv.Itoa(i), "test" + strconv.Itoa(i)}
		}
	}()

	pw.Write(dataCh)

	// 检查文件个数
	files, _ := ioutil.ReadDir(outputDir)
	if len(files) != 46 {
		t.Error("文件个数不对")
	}

	// 检查title
	content, _ := ioutil.ReadFile(path.Join(outputDir, "test.csv"))
	if !strings.Contains(string(content), "no,content") {
		t.Error("title 不对")
	}
}

func cleanWriteCsv(outputDir string) {
	os.RemoveAll(outputDir)
}
