package pbadger

import (
	"testing"
)

func TestWrite(t *testing.T) {
	pb, err := NewPBadgerWriter("/home/wangyubin/tmp/badgerdb/pbadger")
	if err != nil {
		t.Fatal(err)
	}

	var dataCh = make(chan []byte, 10)
	go func() {
		dataCh <- []byte("other01")
		dataCh <- []byte("other02")
		dataCh <- []byte("other03")
		dataCh <- []byte("other04")
		dataCh <- []byte("other05")
		dataCh <- []byte("other06")
		dataCh <- []byte("other07")
		dataCh <- []byte("other08")
		close(dataCh)
	}()

	pb.Write("other-", dataCh)
}
