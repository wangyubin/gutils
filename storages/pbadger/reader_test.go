package pbadger

import (
	"fmt"
	"testing"
)

func TestRead(t *testing.T) {
	pb, err := NewPBadgerReader([]string{"/home/wangyubin/tmp/badgerdb/pbadger"})
	if err != nil {
		t.Fatal(err)
	}

	var dataCh = make(chan []byte, 10)
	go pb.Read("other-", dataCh)

	for data := range dataCh {
		fmt.Printf("data = %s\n", data)
	}
}
