package dgraph

import (
	"encoding/json"
	"fmt"
	"log"
	"testing"
)

const clientName = "th"

type Person struct {
	Uid     string   `json:"uid"`
	Name    string   `json:"name"`
	Age     int      `json:"age"`
	Friends []Person `json:"friends"`
}

func initClients() *DgraphClients {
	cs := NewDgraphClients()
	err := cs.AddDgraphClient(clientName, "119.45.19.28", 9080)
	if err != nil {
		log.Fatal(err)
	}

	return cs
}

func TestDropAll(t *testing.T) {
	cs := initClients()
	err := cs.DropAll(clientName)
	if err != nil {
		t.Fatal(err)
	}
}

func TestDropData(t *testing.T) {
	cs := initClients()

	err := cs.DropData(clientName)
	if err != nil {
		t.Fatal(err)
	}
}

func TestSetSchema(t *testing.T) {
	cs := initClients()

	schema := `
name: string @index(term) .
age: int .
friends: [uid] .

type Person {
  name: string
  age: int
  friends: [Person]
}
`

	err := cs.SetSchema(clientName, schema)
	if err != nil {
		t.Fatal(err)
	}
}

func TestSetJsonData(t *testing.T) {
	cs := initClients()
	p := &Person{
		Name: "test01",
		Age:  10,
		Friends: []Person{{
			Name: "test02",
			Age:  11,
		}, {
			Name: "test03",
			Age:  12,
		}},
	}

	data, err := json.Marshal(p)
	if err != nil {
		t.Fatal(err)
	}

	err = cs.SetJsonData(clientName, data)
	if err != nil {
		t.Fatal(err)
	}
}

func TestSetNquadsData(t *testing.T) {
	cs := initClients()
	nquards := `
_:test01 <name> "test01" .
_:test01 <age> "20" .
_:test02 <name> "test02" .
_:test02 <age> "21" .
_:test03 <name> "test03" .
_:test03 <age> "22" .
_:test01 <friends> _:test02 .
_:test01 <friends> _:test03 .
`
	err := cs.SetNquadsData(clientName, []byte(nquards))
	if err != nil {
		t.Fatal(err)
	}
}

func TestQueryData(t *testing.T) {
	query := `
{
persons(func: eq(name, "test01")){
    name
    age
    uid
  }
}
`

	cs := initClients()
	data, err := cs.QueryData(clientName, query)
	if err != nil {
		t.Fatal(err)
	}

	var persons struct {
		Persons []Person `json:"persons"`
	}

	err = json.Unmarshal(data, &persons)
	if err != nil {
		t.Fatal(err)
	}

	if persons.Persons[0].Name != "test01" || persons.Persons[1].Name != "test01" {
		t.Fail()
	}

	if len(persons.Persons) != 2 {
		t.Fail()
	}
}

func TestQueryDataWithVars(t *testing.T) {
	query := `
query persons($name: string) {
    persons(func: eq(name, $name)){
        name
        age
        uid
    }
}
`
	vars := make(map[string]string)
	vars["$name"] = "test01"

	cs := initClients()
	data, err := cs.QueryDataWithVars(clientName, query, vars)
	if err != nil {
		t.Fatal(err)
	}

	var persons struct {
		Persons []Person `json:"persons"`
	}

	err = json.Unmarshal(data, &persons)
	if err != nil {
		t.Fatal(err)
	}

	if persons.Persons[0].Name != "test01" || persons.Persons[1].Name != "test01" {
		t.Fail()
	}

	if len(persons.Persons) != 2 {
		t.Fail()
	}
}

func TestDeleteJsonData(t *testing.T) {
	cs := initClients()

	data, err := json.Marshal(map[string]string{"uid": "0xfffd8d67d832b997"})
	if err != nil {
		t.Fatal(err)
	}

	err = cs.DeleteJsonData(clientName, data)
	if err != nil {
		t.Fatal(err)
	}
}

func TestDeleteNquadsData(t *testing.T) {
	query := `
{
persons(func: eq(name, "test01")){
    name
    age
    uid
  }
}
`

	cs := initClients()
	resp, err := cs.QueryData(clientName, query)
	if err != nil {
		t.Fatal(err)
	}

	var persons struct {
		Persons []Person `json:"persons"`
	}

	err = json.Unmarshal(resp, &persons)
	if err != nil {
		t.Fatal(err)
	}

	data := fmt.Sprintf(`
<%s> <name> * .
<%s> <age> * .
`, persons.Persons[0].Uid, persons.Persons[0].Uid)

	err = cs.DeleteNquadsData(clientName, []byte(data))
	if err != nil {
		t.Fatal(err)
	}
}
