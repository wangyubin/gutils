#!/bin/bash
#set -x
#******************************************************************************
# @file    : test.sh
# @author  : wangyubin
# @date    : 2020-07-01 16:46:53
#
# @brief   : 测试 dgraph 客户端
# history  : init
#******************************************************************************

# 清理数据
go test -v -run TestDropAll

# 测试 schema创建
go test -v -run TestSetSchema

# 测试 数据创建(JSON)
go test -v -run TestSetJsonData

# 测试 数据创建(Nquards)
go test -v -run TestSetNquadsData

# 测试 数据查询, 不带参数
go test -v -run TestQueryData

# 测试 数据查询, 带参数
go test -v -run TestQueryDataWithVars

# 测试 数据删除(JSON)
go test -v -run TestDeleteJsonData

# 测试 数据删除(Nquards)
go test -v -run TestDeleteNquadsData

# 清理数据
go test -v -run TestDropAll
