package http

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetJson(t *testing.T) {
	resp := "get json response"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		w.Write([]byte(resp))

	}))
	defer ts.Close()

	bs, err := GetJson(ts.URL, nil)
	if err != nil {
		t.Error("get json error")
	}

	if string(bs) != resp {
		t.Error("response error")
	}
}

func TestPostJson(t *testing.T) {
	resp := "post json response"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		w.Write([]byte(resp))

	}))
	defer ts.Close()

	bs, err := PostJson(ts.URL, nil)
	if err != nil {
		t.Error("post json error")
	}

	if string(bs) != resp {
		t.Error("response error")
	}
}
